<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    protected $table='users';
    protected $fillable = [
        'mail_address',
        'name',
        'password',
        'address',
        'phone',
        'role',
    ];
    protected $date = ['deleted_at'];
    protected $perPage = 20;
    /**
     * 
     */
    public function isAdmin() 
    {
        return $this->role === 'admin';
    }
    /**
     * 
     */
    public function isStaff() 
    {
        return $this->role === 'staff';
    }
    /**
     * @param $request
     * @return create $request
     */
    public function addUser(array $request) 
    {
        $user = new User();
        $user->name = $request['name'];
        $user->mail_address = $request['mail_address'];
        $user->address = $request['address'];
        $user->phone = $request['phone'];
        $user->password = Hash::make($request['password']);
        $user->role = $request['role'];
        $user->classroom_id = $request['classroom_id'];
        $user->save();
        return true;
    }
    /**
     * @return users->id=$id
     */
    public function show($id)
    {
        return User::find($id);
    }
    /**
     * @param $id
     * @return user->id=$id
     */
    public function getUser($id)
    {
        return DB::table('users')
            ->where('users.id', $id)
            ->get();
    }
    /**
     * @param $request,$id
     * @return update($request)
     */
    public function updateUser(array $request, $id)
    {
        $user = User::find($id);
        $user->name = $request['name'];
        $user->mail_address = $request['mail_address'];
        $user->address = $request['address'];
        $user->phone = $request['phone'];
        if (empty($request['password'])) {
            $user->password = Hash::make($request['password']);
        }
        $user->role = $request['role'];
        $user->classroom_id = $request['classroom_id'];
        $user->save();
        return true;
    }
    /**
     * @param array $request
     * @return 
     */
    public function searchUser($request)
    {
        if ($request->input('btnSearch')) {
            $search = [];
            $srname = request()->input('srname');
            $sremail_address = request()->input('srmail_address');
            $srphone = request()->input('srphone');
            $sraddress = request()->input('sraddress');
            $srclassroom = $request->input('srclassroom');
            if (isset($srname)) {
                $search['users.name'] = $srname;
            }
            if (isset($srmail_address)) {
                $search['mail_address'] = $srmail_address;
            }
            if (isset($srphone)) {
                $search['phone'] = $srphone;
            }
            if (isset($sraddress)) {
                $search['address'] = $sraddress;
            }
            if (isset($srclassroom)) {
                $search['classroom_id'] = $srclassroom;
            }
            $query = User::query();
            $query->join('classrooms', 'classroom_id', '=', 'classrooms.id');
            if (!empty($search)) {
                foreach ($search as $key => $value) {
                    if (isset($search['phone']) || isset($search['classroom_id'])) {
                        $query->phoneOrClassroomId($key, $value);
                    }
                    $query->mailOrNameOrAddress($key, $value);
                }
                return $query->select('users.id AS id', 'users.name AS user_name', 'classrooms.name AS class_name', 'mail_address', 'phone', 'address', 'users.created_at as user_created_at', 'role')
                ->paginate($this->perPage);
                
            }
        } else {
            return User::query()
            ->join('classrooms', 'classroom_id', '=', 'classrooms.id')
            ->select('users.id AS id', 'users.name AS user_name', 'classrooms.name AS class_name', 'mail_address', 'phone', 'address', 'users.created_at as user_created_at', 'role')
            ->paginate($this->perPage);
        }
    }
    /**
     * 
     */
    public function scopePhoneOrClassroomId($query, $column, $request)
    {
        return $query->where($column, '=' ,$request);
    }
    /**
     * 
     */
    public function scopeMailOrNameOrAddress($query, $column, $request)
    {
        return $query->where($column, 'LIKE', '%' . $request . '%');
    }   
}

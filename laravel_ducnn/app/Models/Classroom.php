<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Classroom extends Model
{
    protected $table = 'classrooms';

    protected $fillable = [
        'name',
    ];
    protected $dates = [
        'deleted_at',
    ];

    public function getClassroom(){
        return DB::table('classrooms')
            ->get();
    }
}

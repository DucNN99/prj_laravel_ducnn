<?php

namespace App\Helpers;

class Helper
{
    public function toUpperCase($keyWord)
    {
        return mb_strtoupper($keyWord);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getmethod() == 'PUT') {
            $id = $this->route('users');
            $rules = [
                'name' => 'required|max:255',
                'mail_address' => 'required|email|unique:users,mail_address,'.$id,
                'password' => 'max:255',
                'confirm_password' => 'same:password',
                'address' => 'nullable|max:255',
                'phone' => 'nullable|regex:/^[0-9\-\+]$/|max:15',
            ];
        } else {
            $rules = [
            'mail_address' => 'required|email|unique:users,mail_address',
            'name' => 'required|max:255',
            'password' => 'required|max:255',
            'password_confirm' =>'required|same:password',
            'address' => 'nullable|max:255',
            'phone' =>'nullable|regex:/^[0-9\-\+]$/|max:15',
            ];
        }
        return $rules;
    }
}


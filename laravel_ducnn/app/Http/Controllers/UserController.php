<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Models\Classroom;
use Auth;
use App\Jobs\SendEmail;
use App\Mail\TestEmail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Events\CreatedUser;

class UserController extends Controller
{
    public $user;
    public $classroom;
    /**
     * Dependency Injection model User
     */
    public function __construct(User $user,Classroom $classroom)
    {
        $this->user = $user;
        $this->classroom = $classroom;
        $this->middleware('check_login');
        $this->middleware('classable')->only('index', 'create', 'store');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin');
        $classroom = $this->classroom->getClassroom();
        return view('add_user', compact('classroom'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index(Request $request)
    {
        $user = $this->user->searchUser($request);
        $classroom = $this->classroom->getClassroom();
        return view('list_user', compact('user', 'classroom'));
    }  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        // $this->user->addUser($request->all());
        flash('Thêm mới thành công');
        $details = [
            'mail_address' => $request->mail_address,
            'name' => $request->name
        ];
        event(new CreatedUser($details));
        return redirect(route('users.index'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user->show($id);
        $classroom = $this->classroom->getClassroom();
        return view('update_user', compact('user', 'classroom'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->getUser($id);
        $classroom = $this->classroom->getClassroom();
        return view('users.form', compact('user', 'classroom'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUserRequest $request, $id)
    {
        $this->authorize('isAdmin');
        $this->user->updateUser($request->all(), $id);
        flash('Cập nhật thành công');
        return redirect(route('users.index'));
    }
}


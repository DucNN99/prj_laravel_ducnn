@extends('layouts.app')
@section('title', 'Edit User')
@section('content')
<div class="container">
    <h1>Edit User</h1>
    <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
        @include('layouts.users.form')
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    </form>
</div>
@endsection

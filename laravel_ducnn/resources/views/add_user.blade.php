@extends('layouts.app')
@section('title','Add user')
@section('content')
    <div class="container">
        <h1>Add a new User</h1>
        <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
            @csrf
            @include('layouts.users.form')         
        </form>
    </div>
@endsection

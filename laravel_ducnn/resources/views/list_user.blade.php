@extends('layouts.app')
@section('title','List users')
@section('content')
<form action="{{ route('users.index') }}" method="get">
    <div class="row">
        <div class="col-2">
            <input type="text" class="form-control" name="srname" placeholder="Search for Name">
        </div>
        <div class="col-2">
            <input type="text" class="form-control" name="srmail_address" placeholder="Search for Email">
        </div>
        <div class="col-2">    
            <input type="text" class="form-control" name="sraddress" placeholder="Search for Address">
        </div>
        <div class="col-2"> 
            <input type="text" class="form-control" name="srphone" placeholder="Search for Phone">
        </div>
        <div class="col-2">
            <select name="srclassroom" class="browser-default custom-select">
                @foreach($classroom as $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-2"> 
            <input type="submit" value="Search" name="btnSearch" class="btn btn-primary">
        </div>
    </div>
</form>
<hr>
<br>
@include('flash::message')
<table class="table table-hover">
    <tr>
        <th>STT</th>
        <th>Name</th>
        <th>Class</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Create At</th>
        @can('isAdmin')
        <th>Roles</th>
        <th>Action</th>
        @endcan
    </tr>
    @foreach ( $user as $key => $value)
    <tr>
        <td>
            {{ $user->perPage() * ($user->currentPage() -1) + $key + 1}}
        </td>
        <td>
            {{ \HelperFacade::toUpperCase($value->user_name) }}
        </td>
        <td>
            {{ $value->class_name }}
        </td>
        <td>
            {{ $value->mail_address }}
        </td>
        <td>
            {{ $value->phone }}
        </td>
        <td>
            {{ $value->address }}
        </td>
        <td>
            {{ $value->user_created_at }}
        </td>
        @can('isAdmin')
        <td>
            {{ $value->role }}
        </td>
        <td>
            <button type="button" class="btn btn-default">
                <a href="{{ route('users.update', $value->id) }}">Update</a>
            </button>
        </td>
        @endcan
    </tr>
    @endforeach
    <tr>
        <td colspan="5">
            {{ $user->links() }}
        </td>
    </tr>
</table>
@endsection

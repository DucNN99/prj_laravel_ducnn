<script>
$(document).ready(function() {
    @if ($errors->first('mail_address'))
        $("#ipemail").css("border", "1px solid red");
        $("#lbemail").css("color", "red");
    @endif

    @if ($errors->first('name'))
        $("#ipusername").css("border", "1px solid red");
        $("#lbusername").css("color", "red");
    @endif

    @if ($errors->first('address'))
        $("#ipaddress").css("border", "1px solid red");
        $("#lbaddress").css("color", "red");
    @endif

    @if ($errors->first('phone'))
        $("#ipphone").css("border", "1px solid red");
        $("#lbphone").css("color", "red");
    @endif

    @if ($errors->first('password'))
        $("#ippassword").css("border", "1px solid red");
        $("#lbpassword").css("color", "red");
    @endif

    @if ($errors->first('password_confirm'))
        $("#ipconfirm").css("border", "1px solid red");
        $("#lbconfirm").css("color", "red");
    @endif
});
</script>
    <div class="row">
        <div class="col">
            <label id="lbusername">Username :</label>
            <input type="text" name="name" id="ipusername" class="form-control" value="{{ old('name', isset($user->name) ? $user->name : null) }}">
            <span>
                @error('name') {{ $errors->first('name') }} @enderror
            </span>
         </div>
    </div>

    <div class="row">
        <div class="col">
            <label for="" id="lbemail">Email :</label>
            <input type="text" name="mail_address" id="ipemail" class="form-control" value="{{ old('mail_address', isset($user->mail_address) ? $user->mail_address : null) }}">
            <span>
                @error('mail_address') {{ $errors->first('mail_address') }} @enderror
            </span>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <label for="" id="lbpassword">Password :</label>
            <input type="password" name="password" id="ippassword" class="form-control">
            <span>
                @error('password') {{ $errors->first('password') }} @enderror
            </span>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <label for="" id="lbconfirm">Confirm Password :</label>
            <input type="password" name="password_confirm" id="ipconfirm" class="form-control">
            <span>
                @error('password_confirm') {{ $errors->first('password_confirm') }} @enderror
            </span>
        </div>
    </div>
            
    <div class="row">
        <div class="col">
            <label for="" id="lbaddress">Address :</label>
            <input type="text" name="address" id="ipaddress" class="form-control" value="{{ old('address', isset($user->address) ? $user->address : null) }}">
            <span>
                @error('address') {{ $errors->first('address') }} @enderror
            </span>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <label for="" id="lbphone">Phone :</label>
            <input type="text" name="phone" id="ipphone" class="form-control" value="{{ old('phone', isset($user->phone) ? $user->phone : null) }}">
            <span>
                @error('phone') {{ $errors->first('phone') }} @enderror
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <label for="role" id="lbrole">Role : </label>
            <select name="role" id="role" class="browser-default custom-select">
                <option selected>Select role...</option>
                <option value="admin">Administrator</option>
                <option value="staff">Staff</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <label for="class" id="lbclass">Class : </label>
            <select class="browser-default custom-select" name="classroom_id">
                <option selected>Select classroom...</option>
                @foreach($classroom as $value)
                <option value="{{ old('id', isset($value->id) ? $value->id : selected) }}">{{ $value->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-6">
            <input type="submit" value="@isset($user->id) Update @else Add @endif" class="btn btn-primary">
        </div>
    </div>  

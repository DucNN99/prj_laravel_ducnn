<br>
<header>
@can('isAdmin')
<div id="menu">
    <ul>
        <li><a href="{{ route('users.index') }}" class="btn btn-primary">List</a></li>
        <li><a href="{{ route('users.create') }}" class="btn btn-success">Add User</a></li>
    </ul>
</div>
@endcan
</header>




<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>@yield('title')</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        span {
            color: red ;
        }
        #menu ul {
            list-style-type: none;
        }
        #menu li {
            color: #f1f1f1;
            display: inline-block;
            width: 150px;
            height: 40px;
            line-height: 20px;
            margin : 0px;
        }
        #menu ul {
            list-style-type: none;
            overflow: hidden;
            width: 100%;
        }
        #menu a {
            text-decoration: none;
            display: block;
        }
        #menu a:hover {
            background: white;
            color: black;
        }
        footer {
            background : black;
            color : white;
            text-align : center;
            height : 60px;
        }
        h2 {
            padding-top : 15px;
        }
    </style> 
</head>

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Classroom;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Models\Classroom::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
    ];
});
